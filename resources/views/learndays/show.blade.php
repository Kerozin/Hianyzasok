@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col"></div>
        <div class="col-6">
            <div class="card border-secondary bg-white">
                <div class="card-body text-white">
                    <form action="{{route("attendances.massupdate",$learnday)}}" method="POST">
                        @csrf
                        @method("put")
                        <div class="table-responsive">
                            <table class="table table">
                                <thead>
                                    <tr>
                                        <th scope="col">Student name</th>
                                        <th scope="col">Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($learnday->attendances as $item)
                                        <tr class="">
                                            <td scope="row">{{ $item->student->name }}</td>
                                            <td>{{ $item->status }}</td>
                                            <td>
                                                <div class="btn-group" role="group"
                                                    aria-label="Basic radio toggle button group">
                                                    <input type="radio" class="btn-check" name="status{{ $item->student->id }}" id="status{{ $item->student->id }}1" value="jelen" autocomplete="off" checked>
                                                    <label class="btn btn-outline-primary"
                                                        for="status{{ $item->student->id }}1">jelen</label>

                                                    <input type="radio" class="btn-check" name="status{{ $item->student->id }}" id="status{{ $item->student->id }}2" value="hiányzó" autocomplete="off">
                                                    <label class="btn btn-outline-primary"
                                                        for="status{{ $item->student->id }}2">hiányzó</label>

                                                    <input type="radio" class="btn-check" name="status{{ $item->student->id }}" id="status{{ $item->student->id }}3" value="keresőképtelen" autocomplete="off">
                                                    <label class="btn btn-outline-primary"
                                                        for="status{{ $item->student->id }}3">keresőképtelen</label>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>

                            <div class="text-end">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>
@endsection
