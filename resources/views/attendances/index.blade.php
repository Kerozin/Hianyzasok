@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        <div class="col-2"></div>
        <div class="col-8">
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                    <strong>Holy guacamole!</strong>
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
            <div class="table-responsive-xl">
                <table class="table table-dark" id="studentTable">
                    <thead>
                        <tr>
                            <th scope="col">Class</th>
                            <th scope="col">Learn Day</th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($attendances as $item)
                            <tr class="">
                              <td scope="row">{{ $item->learnday->course->name}}</td>  
                                <td>{{ $item->learnday->date }}</td> 
                                <td>
                                    <form action="{{ route('attendances.edit', $item) }}" method="GET">
                                        @csrf
                                        <button type="submit" class="btn btn-warning">Edit</button>
                                    </form>
                                </td>
                                <td>

                                    <form action="{{ route('attendances.show', $item) }}" method="GET">
                                        @csrf
                                        <button type="submit" class="btn btn-info">Show data</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-2"></div>
    </div>
@endsection
