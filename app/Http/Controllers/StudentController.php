<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\Course;
use App\Models\Student;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $students = Student::all();
        return view("students.index", ['students' => $students]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Student $student)
    {
        $courses = Course::all();
        return view('students.create' , ['student' => $student, 'courses' => $courses]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStudentRequest $request)
    {
        
        $student = new Student(
            [
                "name" => $request->name,
                "birthday" => $request->birthday,
                "course_id" => $request->course_id
            ]
        );

        $student->save();

        return back()->with("success", "Student added successfully.");

    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        $students = Student::all();
        return view('students.show', ['student' => $student ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Student $student)
    {
        $courses = Course::all();
        return view('students.edit', ['student' => $student, 'courses' => $courses]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {
        $student->update($request->all());

        return redirect()->route('students.index')->with('success', 'The student was updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student)
    {
        
        $student->delete();
        return back()->with('success', 'Successfully deleted the student: ' . $student->name . '.');

    }

    public function show_deleted()
    {
        $deleted_students = Student::onlyTrashed()->get();
        return view('students.show_deleted', ['students' => $deleted_students]);
    }

    public function restore(Student $student)
    {
        $student->restore();
        return back()->with('success', '' . $student->name . ' was successfully restored.');
    }

}
