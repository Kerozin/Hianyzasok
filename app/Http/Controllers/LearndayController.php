<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLearnDayRequest;
use App\Http\Requests\UpdateLearnDayRequest;
use App\Models\Attendance;
use App\Models\Course;
use App\Models\LearnDay;
use App\Models\Student;

class LearnDayController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $learndays = LearnDay::all();

        /* return redirect()->action([AttendanceController::class, 'learnday.index'], ['learndays' => $learndays]); */
       return view("learndays.index", ['learndays' => $learndays]); 
    }

    
    /**
     * Show the form for creating a new resource.
     */
    public function create(LearnDay $learnday)
    {
        $courses = Course::all();
        return view('learndays.create' , ['learndays' => $learnday, 'courses' => $courses]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLearnDayRequest $request)
    {
        $learnday = new LearnDay(
            [
                "title" => $request->title,
                "date" => $request->date,
                "course_id" => $request->course_id
            ]
        );

        $learnday->save();

        return redirect()->route('attendances.store',$learnday);
    }

    /**
     * Display the specified resource.
     */
    public function show($learnday)
    {
        $learnday = LearnDay::find($learnday);
        return view('learndays.show', ['learnday' => $learnday]);

        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($learnday)
    {
        $courses = Course::all();

        $learnday = LearnDay::find($learnday);
        return view('learndays.edit', ['learndays' => $learnday, 'courses' => $courses]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLearnDayRequest $request, LearnDay $learnday)
    {
        $learnday->update($request->all());

        return redirect()->route('learndays.index')->with('success', 'The learn day was updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {

        $learnday = LearnDay::find($id);
        $learnday->delete();

        Attendance::where('learnday_id', $id)->delete();

        return back()->with('success', 'Successfully deleted the learn day: ' . $learnday->title . '.');
    }


    public function show_deleted()
    {
        $deleted_days = LearnDay::onlyTrashed()->get();
        return view('learndays.show_deleted', ['learndays' => $deleted_days]);
    }

    public function restore($learnday)
    {
        $learnday = LearnDay::withTrashed()->find($learnday);
        $learnday->restore();
        return back()->with('success', '' . $learnday->title . ' was successfully restored.');
    }
}